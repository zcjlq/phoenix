package com.gitee.pifeng.monitoring.server.business.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gitee.pifeng.monitoring.server.business.server.entity.MonitorTcpIp;

/**
 * <p>
 * TCP/IP信息服务接口
 * </p>
 *
 * @author 皮锋
 * @custom.date 2022/1/11 16:14
 */
public interface ITcpIpService extends IService<MonitorTcpIp> {
}
